import java.util.*;
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter numbers");
        String numbers = scanner.nextLine();
        String[] arrayOfNumbers = numbers.split("\\s+");
        System.out.println(findShortestAndLongest(arrayOfNumbers));
        System.out.println(analyzeDigits(arrayOfNumbers));
        System.out.println(findNumberWithIncreasingDigits(arrayOfNumbers));
        scanner.close();
    }

    public static String findShortestAndLongest(String[] arrayOfNumbers) {
        int minimumLength = arrayOfNumbers[0].length();
        int minimumLengthNumber = Integer.valueOf(arrayOfNumbers[0]);
        int maximumLength = arrayOfNumbers[0].length();
        int maximumLengthNumber = Integer.valueOf(arrayOfNumbers[0]);

        for (int i = 0; i < arrayOfNumbers.length; i++) {
            if (arrayOfNumbers[i].length() < minimumLength) {
                minimumLength = arrayOfNumbers[i].length();
                minimumLengthNumber = Integer.valueOf(arrayOfNumbers[i]);
            }
            if (arrayOfNumbers[i].length() > maximumLength) {
                maximumLength = arrayOfNumbers[i].length();
                maximumLengthNumber = Integer.valueOf(arrayOfNumbers[i]);
            }
        }
        return "1.The shortest number is " + minimumLengthNumber + " and its length is " + minimumLength + "\n2.The longest number is " + maximumLengthNumber + " and its length is " + maximumLength ;
    }


// I combined questions 3 and 5 because they are similar in a way that I have to look at digits

    public static String analyzeDigits(String[] arrayOfNumbers) {
        int leastDifferentDigits = Integer.MAX_VALUE;
        int differentdigits = 0;
        String allDifferentDigits = "NONE";
        String leastDifferentDigitsNumber = arrayOfNumbers[0];

        for (int i = 0; i < arrayOfNumbers.length; i++) {
            Set<Character> digits = new HashSet<Character>();
            for (int j =0; j< arrayOfNumbers[i].length(); j++){
              if( !digits.contains(arrayOfNumbers[i].charAt(j)) ){
                  digits.add(arrayOfNumbers[i].charAt(j));
                  differentdigits++;
              }
           }
            if (differentdigits < leastDifferentDigits){
                leastDifferentDigits = differentdigits;
                leastDifferentDigitsNumber = arrayOfNumbers[i];
            }
            if (digits.size() == arrayOfNumbers[i].length() && allDifferentDigits == "NONE"){
            allDifferentDigits = arrayOfNumbers[i];
            }
            differentdigits = 0;
        }

        return "3.The number with least number of different digits is "+ leastDifferentDigitsNumber+ "\n5.The number with all different digits is "+allDifferentDigits ;
    }



    public static String findNumberWithIncreasingDigits(String[] arrayOfNumbers){
    String strictlyIncreasingNumber = "NONE";

    for (int i =0; i< arrayOfNumbers.length; i++){
        int count =1;
        for (int j =0; j< arrayOfNumbers[i].length()-1; j++){
            if ( arrayOfNumbers[i].charAt(j) < arrayOfNumbers[i].charAt(j+1) ){
                count++;
            }
            else {
                break;
            }
        }
        if (count == arrayOfNumbers[i].length() && strictlyIncreasingNumber == "NONE" ){
            strictlyIncreasingNumber = arrayOfNumbers[i];
        }
    }
    return "4.The number with strictly increasing digits is "+ strictlyIncreasingNumber;
    }
}